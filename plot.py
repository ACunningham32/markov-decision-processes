import matplotlib.pyplot as plt


# VI
vi_discounts = [0.5, 0.6, 0.75, 0.8, 0.85, 0.9, 0.95, 0.99, 1.0]
vi_iterations = [28,38,67,87,119,163,282,1175,1000]
vi_time = [0.8382120132446289,1.2857749462127686,1.966181993484497,2.7014291286468506,3.537447214126587,5.340415000915527,8.938289880752563,39.31763315200806,31.6909499168396]

pi_discounts = [0.5, 0.6, 0.75, 0.8, 0.85, 0.9, 0.95, 0.99]
pi_iterations = [17,20,24,27,31,43,62,201]
pi_time = [11.01904821395874,11.25742793083191,12.8189058303833,15.735939979553223,16.355007886886597,24.490766048431396,37.118892669677734,106.68241214752197]

ax = plt.gca()
ax2 = ax.twinx()

ax.plot(vi_discounts, vi_iterations, 'r--', label="VI iterations")
ax.plot(pi_discounts, pi_iterations, 'b--', label="PI iterations")
ax.set_xlabel(u'\u03B3')
ax.set_ylabel('Iterations')
ax_handles, ax_labels = ax.get_legend_handles_labels()

ax2.plot(vi_discounts, vi_time, 'ro', label="VI time")
ax2.plot(vi_discounts, vi_time, 'r-')
ax2.plot(pi_discounts, pi_time, 'bo', label="PI time")
ax2.plot(pi_discounts, pi_time, 'b-')
ax2.set_ylabel('Time')
ax2_handles, ax2_labels = ax2.get_legend_handles_labels()

handles = ax_handles + ax2_handles
labels  = ax_labels + ax2_labels

plt.legend(handles, labels, loc='upper left')
plt.show()