import sys
import numpy as np
import mdptoolbox
import matplotlib.pyplot as plt

def get_transition_rewards():
    transition = np.zeros((2, 23, 23))
    total_cards = 52.

    # make transition[0] "Hit"
    for i in range(len(transition[0])):
        probs = transition[0][i]

        remaining_flag = False
        prob_single_card = 4/total_cards
        for j in range(1, 10):
            if i + j < len(probs):
                probs[i + j] = prob_single_card
            else:
                remaining_flag = True

        if i+ 10 < len(probs):
            probs[i + 10] = prob_single_card * 4
        else:
            remaining_flag = True

        if remaining_flag:
            remaining_probs = 1 - sum(probs[:-1])
            probs[len(probs) - 1] = remaining_probs

        # output = ''
        # for p in probs:
        #     output = output[:] + ' {0:.4f} '.format(p)

        # print(i, output)

    # make transition[1] "Hold"
    for i in range(len(transition[1])):
        probs = transition[1][i]
        probs[len(probs) - 1] = 1.0

        # output = ''
        # for p in probs:
        #     output = output[:] + ' {0:.4f} '.format(p)

        # print(i, output)

    rewards = np.zeros((23, 2))
    rewards[0]  = [0., 0.]
    rewards[1]  = [0., 1.]
    rewards[2]  = [0., 2.]
    rewards[3]  = [0., 3.]
    rewards[4]  = [0., 4.]
    rewards[5]  = [0., 5.]
    rewards[6]  = [0., 6.]
    rewards[7]  = [0., 7.]
    rewards[8]  = [0., 8.]
    rewards[9]  = [0., 9.]
    rewards[10] = [0.,10.]
    rewards[11] = [0.,11.]
    rewards[12] = [0.,12.]
    rewards[13] = [0.,13.]
    rewards[14] = [0.,14.]
    rewards[15] = [0.,15.]
    rewards[16] = [0.,16.]
    rewards[17] = [0.,17.]
    rewards[18] = [0.,18.]
    rewards[19] = [0.,19.]
    rewards[20] = [0.,20.]
    rewards[21] = [0.,21.]
    rewards[22] = [0., 0.]

    return transition, rewards

if __name__ == "__main__":
    T, R = get_transition_rewards()

    if len(sys.argv) > 1:
        if '-vi' in sys.argv:
            vi = mdptoolbox.mdp.ValueIteration(T, R, 0.99)
            vi.setVerbose()
            vi.run()
            print(vi.V)
            print(vi.iter)
            print(vi.time)
            for p in range(len(vi.policy)):
                print(p, vi.policy[p])

        elif '-pi' in sys.argv:
            pi = mdptoolbox.mdp.PolicyIteration(T, R, 0.99)
            pi.setVerbose()
            pi.run()
            print(pi.V)
            print(pi.iter)
            print(pi.time)
            for p in range(len(pi.policy)):
                print(p, pi.policy[p])

        elif '-q' in sys.argv:
            q = mdptoolbox.mdp.QLearning(T, R, 0.99, n_iter=100000)
            q.setVerbose()
            q.run()
            print(q.V)
            print(q.time)
            print(q.mean_discrepancy)
            for p in range(len(q.policy)):
                print(p, q.policy[p])

            # for _q in range(len(q.Q)):
            #     print(_q, q.Q[_q])

            # plt.plot(q.mean_discrepancy)
            # plt.show()

        else:
            print('Please specify which algorithm to run: -vi, -pi or -q')