#How to run:

`pip install -r requirements.txt`

##To run blackjack:

###Value Iteration
```python blackjack.py -vi```
###Policy Iteration
```python blackjack.py -pi```
###Q-Learning
```python blackjack.py -q```

##To run maze:
###Value Iteration
```python maze.py -vi```
###Policy Iteration
```python maze.py -pi```
###Q-Learning
```python maze.py -q```